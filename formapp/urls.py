
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('', schedule, name ='schedule'),
    path('create/', schedule_create, name ='schedule_create'),
    path('deleteall/', schedule_deleteall, name = 'schedule_deleteall'),
    path('delete/<int:id>/', schedule_delete, name='schedule_delete'),


]
