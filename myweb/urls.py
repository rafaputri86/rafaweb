
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name = 'index'),
    path('portofolio', portofolio, name ='portofolio'),
    path('caneat', caneat, name ='caneat'),
    path('hr', hr, name = 'hr'),
    path('edufun', edufun, name ='edufun'),
    path('skilvul', skilvul, name ='skilvul'),
]
