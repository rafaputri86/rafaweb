from django.shortcuts import render

def index(request):
	return render(request, 'index.html')

def caneat(request):
    return render(request, 'caneat.html')

def portofolio(request):
    return render(request, 'portofolio.html')

def hr(request):
    return render(request, 'hr.html')

def edufun(request):
    return render(request, 'edufun.html')
    
def skilvul(request):
    return render(request, 'skilvul.html')



